<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function muni_export_content_types_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_data
  $groups['muni-group_data'] = array(
    'group_type' => 'standard',
    'type_name' => 'muni',
    'group_name' => 'group_data',
    'label' => 'data',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '29',
        'label' => 'hidden',
        'teaser' => array(
          'format' => 'simple',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'simple',
          'exclude' => 0,
        ),
        'cck_blocks' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '29',
    'fields' => array(
      '0' => 'field_designation',
      '1' => 'field_region_administrative',
      '2' => 'field_pop',
      '3' => 'field_gentile',
      '4' => 'field_date_constitution',
      '5' => 'field_division_territoriale',
      '6' => 'field_recensement_canada',
      '7' => 'field_communaute_metropolitaire',
      '8' => 'field_maire',
      '9' => 'field_conseillers',
      '10' => 'field_responsables',
      '11' => 'field_election_provinciale',
      '12' => 'field_changement_regime',
      '13' => 'field_prochaine_election',
      '14' => 'field_mode_election',
      '15' => 'field_area',
      '16' => 'field_email',
      '17' => 'field_siteweb',
      '18' => 'field_toponymie',
      '19' => 'field_date_topo',
      '20' => 'field_source_toponymie',
      '21' => 'field_specifique_toponymie',
      '22' => 'field_generique_toponymie',
      '23' => 'field_geocode',
      '24' => 'field_mrc',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('data');

  return $groups;
}
