<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function muni_export_content_types_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: primary-links
  $menus['primary-links'] = array(
    'menu_name' => 'primary-links',
    'title' => 'Liens primaires',
    'description' => 'Les lien primaires sont utilisés pour représenter les rubriques principales d\'un site. Selon les thèmes, ils peuvent par exemple correspondre à des onglets affichés en entête de page.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Les lien primaires sont utilisés pour représenter les rubriques principales d\'un site. Selon les thèmes, ils peuvent par exemple correspondre à des onglets affichés en entête de page.');
  t('Liens primaires');


  return $menus;
}
